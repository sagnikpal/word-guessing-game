import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'gamesmenu.dart';
import 'updateWidget.dart';

void main(){
  runApp(HomePage());
}

class HomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.cyan,
        accentColor: Colors.red,
        brightness: Brightness.dark,
        fontFamily: 'Pangolin',
        snackBarTheme: SnackBarThemeData(
          backgroundColor: Colors.white10,
          behavior: SnackBarBehavior.floating,
          contentTextStyle: TextStyle(
            fontFamily: 'Pangolin',
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          elevation: 0,
        ),
      ),
      home: menuPage(),
    );
  }
}

class menuPage extends StatelessWidget {


  int idx = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Learning Curve App'),
      ),
      body: Container(
        padding: const EdgeInsets.all(20.0),
//        decoration: new BoxDecoration(
//          color: Colors.black12,
//          image: new DecorationImage(
//            fit: BoxFit.cover,
//            colorFilter: new ColorFilter.mode(Colors.black12.withOpacity(0.4), BlendMode.dstATop),
//            image: AssetImage('images/splash.jpg'),
//          ),
//        ),
        child: Center(
          child: Column(
            children: <Widget>[
              SizedBox(height: 30.0,),
              Card(
                color: Colors.white24,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: InkWell(
                  splashColor: Colors.blue.withAlpha(30),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Menu(idx),
                      ),
                    );
                  },
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: 320,
                        height: 300,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Center(child: Image(image: AssetImage('images/game4.png'), color: Colors.black26, height: 180.0,)),
                            Text('Games', style: TextStyle(fontSize: 45.0, color: Colors.black26, fontWeight: FontWeight.bold),)
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 30.0,),
              Card(
                color: Colors.white24,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: InkWell(
                  splashColor: Colors.blue.withAlpha(30),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => updateWidget(),
                      ),
                    );
                  },
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: 320,
                        height: 300,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Center(child: Image(image: AssetImage('images/download.png'), color: Colors.black26, height: 200.0,)),
                            SizedBox(height: 15.0,),
                            Text('Update', style: TextStyle(fontSize: 45.0, color: Colors.black26, fontWeight: FontWeight.bold),),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

