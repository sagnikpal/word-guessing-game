import 'package:flutter/material.dart';
import 'play.dart';
import 'game.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'dart:async';


class Result extends StatelessWidget {
  final Game game;
  final int idx;
  Result.fromData(
      this.game, this.idx
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Result"),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            _getResult(),
            Text(
              "Word: " + game.secretWord,
              style: TextStyle(fontSize: 20),
            ),
//            _getStars(),
            FlatButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Next Word",
                  style: TextStyle(fontSize: 40,),
                ),
              ),
              color: Colors.blueGrey,
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return Splash(idx+1);
                }));
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _getResult() {
    if (game.isWordGuessed()) {
      return Column(
        children: <Widget>[
          Image.asset(
            'images/unnamed.png',
            height: 100,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: Text(
              game.desc,
              style: TextStyle(fontSize: 32),
            ),
          ),
        ],
      );
    } else {
      return Center(
        child: Column(
          children: <Widget>[
            Image.asset(
              'images/unnamed.png',
              height: 100,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 65.0, vertical: 20.0),
              child: Text(
                game.desc,
                style: TextStyle(fontSize: 32),
              ),
            ),
          ],
        ),
      );
    }
  }
}


class Spl extends StatefulWidget {

  final Game game;
  int idx;
  Spl.fromData(this.game, this.idx);
  @override
  _SplashState createState() => _SplashState(game, idx);
}

class _SplashState extends State<Spl> {

  Game game;
  Game g;
  int idx;
  _SplashState(this.game, this.idx);
  @override
  void initState() {
    // TODO: implement initState
    Timer(Duration(milliseconds: 2100 ), (){
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) {
        return Result.fromData(game, idx);
      }));
    });
  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      backgroundColor: Colors.blueGrey.withOpacity(0.7),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Processing Result...',
              style: TextStyle(fontSize: 25.0),
            ),
            SizedBox(height: 20.0,),
            Container(
              child: SpinKitWave(
                color: Colors.white,
                size: 80.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}


