import 'dart:async';
import 'package:flutter/material.dart';
import 'play.dart';
import 'updateWidget.dart';
import 'storyHome.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.cyan,
        accentColor: Colors.red,
        brightness: Brightness.dark,
        fontFamily: 'Pangolin',
        snackBarTheme: SnackBarThemeData(
          backgroundColor: Colors.white10,
          behavior: SnackBarBehavior.floating,
          contentTextStyle: TextStyle(
            fontFamily: 'Pangolin',
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          elevation: 0,
        ),
      ),
      home: Splash(),
    );
  }
}


//
//
//class Splash extends StatefulWidget {
//
//  @override
//  _SplashState createState() => _SplashState();
//}
//
//class _SplashState extends State<Splash> {
//
//  @override
//  void initState() {
//    // TODO: implement initState
//    Timer(Duration(seconds: 10), (){
//      Navigator.pushReplacement(
//        context,
//        MaterialPageRoute(
//          builder: (context) => Play(),
//        ),
//      );
//    });
//  }
//
//
//
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
////      backgroundColor: Colors.blueGrey.withOpacity(0.7),
//      body: Container(
//        decoration: new BoxDecoration(
//          color: Colors.black12,
//          image: new DecorationImage(
//            fit: BoxFit.cover,
//            colorFilter: new ColorFilter.mode(Colors.black12.withOpacity(0.2), BlendMode.dstATop),
//            image: AssetImage('images/splash.jpg'),
//          ),
//        ),
//        child: Column(
//          mainAxisAlignment: MainAxisAlignment.spaceBetween,
//          children: <Widget>[
//            SizedBox(),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.center,
//              children: <Widget>[
////                Text('Learning Curve',
////                  style: TextStyle(fontSize: 45.0, color: Colors.white, fontWeight: FontWeight.bold),
////                ),
//                ]
//            ),
//            Column(
//              children: <Widget>[
//                Text('Developed by Code2Create', style: TextStyle(fontSize: 17.0, color: Colors.white, fontWeight: FontWeight.bold),),
//                // Text('Navnomesh', style: TextStyle(fontSize: 17.0, color: Colors.white, fontWeight: FontWeight.bold),),
//                SizedBox(height: 40.0,),
//              ],
//            ),
//
//          ],
//        ),
//      ),
//    );
//  }
//}
