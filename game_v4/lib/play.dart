import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:game/game.dart';
import 'result.dart';
import 'dart:async';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Play extends StatefulWidget {

  final Game game;
  int idx;
  Play.fromData(
      this.game, this.idx);

  @override
  _PlayState createState() => _PlayState(game, idx);
}

class _PlayState extends State<Play> {

  final Game game;
  int idx;
  String lives;
  _PlayState(this.game, this.idx);
  @override
  void initState() {
    super.initState();
    lives = '6';
  }

  Widget _getImage(){
    if(game.bytes == null){
      return Image(
        image: AssetImage('images/source.gif'),
      );
    }
    else{
      return ClipRRect(
        borderRadius: BorderRadius.circular(15.0),
        child: Image.memory(game.bytes, fit: BoxFit.cover,),
      );
//      return new Image.memory(game.bytes, fit: BoxFit.cover,);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text("Guess The Word"),
            SizedBox(width: 95.0,),
            Text("Lives: " + lives),
          ],
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                color: Colors.blueGrey.withOpacity(0.9),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                padding: EdgeInsets.symmetric(horizontal: 105.0, vertical: 10.0),
                child: Text(
                  'Change Word',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 25.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                onPressed: (){
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Splash(idx+1),
                    ),
                  );
                },
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 9.0, vertical: 17.0),
                height: 300.0,
                width: 400.0,
                decoration: BoxDecoration(
                  color: Colors.blueGrey.withOpacity(0.3),
                  borderRadius: BorderRadius.all(Radius.circular(15.0))
                ),
                child: _getImage(),
              ),
              Text(
                game.displayWord,
                style: TextStyle(fontSize: 45),
              ),
              SizedBox(
                height: 15.0,
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: _getKeyPad(),
              ),
              SizedBox(
                height: 5.0,
              ),
            ],
          ),
        ),
      ),
    );
  }


  Widget _getKeyPad() {
    List<List<String>> letters = [
      ['A', 'B', 'C', 'D', 'E', 'F', 'G'],
      ['H', 'I', 'J', 'K', 'L', 'M', 'N'],
      ['O', 'P', 'Q', 'R', 'S', 'T', 'U'],
      ['V', 'W', 'X', 'Y', 'Z'],
    ];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment:  MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 6.0),
          child: _getRow(0, letters),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 6.0),
          child: _getRow(1, letters),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 6.0),
          child: _getRow(2, letters),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 56.0),
          child: _getRow(3, letters),
        ),

      ],
    );
  }

  Row _getRow(int rowIndex, List<List<String>> letters) {
    List<Widget> row = [];
    for (int i = 0; i < letters[rowIndex].length; i++) {
      row.add(_getLetterButton(letters[rowIndex][i]));
    }
    return Row(
      children: row,
    );
  }

  Widget _getLetterButton(String letter) {
    return Padding(
      padding: const EdgeInsets.all(3.0),
      child: Builder(
        builder: (BuildContext context) {
          return GestureDetector(
            child: Container(
              width: MediaQuery.of(context).size.width / 9.0,
              height: 45,
              alignment: Alignment.center,
              child: Text(
                letter,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 30.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(15)),
                color: _getLetterColor(letter),
              ),
            ),
            onTap: () {
              if (game.displayWordList.contains(letter) ||
                  game.wrongLettersGuessed.contains(letter)) {
                Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text(
                    "Already Guessed",
                    textAlign: TextAlign.center,
                  ),
                  duration: Duration(milliseconds: 200),
                ));
              } else {
                Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text(
                    letter,
                    textAlign: TextAlign.center,
                  ),
                  duration: Duration(milliseconds: 200),
                ));
                setState(() {
                  game.guessLetter(letter);
                });
                if (game.isWordGuessed() || game.hasLost()) {
                  Navigator.of(context).pop();
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return Spl.fromData(game, idx);
                  }));
                }
              }
              setState(() {
                lives = (6 - game.wrongLettersGuessed.length).toString();
              });
            },
          );
        },
      ),
    );
  }

  Color _getLetterColor(String letter) {
    if (game.displayWordList.contains(letter)) {
      return Colors.green;
    } else if (game.wrongLettersGuessed.contains(letter)) {
      return Colors.red;
    } else {
      return Colors.blueGrey;
    }
  }
}



class Splash extends StatefulWidget {

  final int idx;
  Splash(this.idx);
  @override
  _SplashState createState() => _SplashState(idx);
}

class _SplashState extends State<Splash> {

  int idx;
  _SplashState(this.idx);
  @override
  void initState() {
    Game game = Game(idx);
    // TODO: implement initState
    Timer(Duration(milliseconds: 2100 ), (){
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) {
        return Play.fromData(game, idx);
      }));
    });
  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      backgroundColor: Colors.blueGrey.withOpacity(0.7),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Loading Game...',
              style: TextStyle(fontSize: 25.0),
            ),
            SizedBox(height: 20.0,),
            Container(
              child: SpinKitWave(
                color: Colors.white,
                size: 80.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}




