import 'package:flutter/material.dart';
import 'play.dart';

class Intro extends StatelessWidget {

  int idx;
  Intro(int idx){
    this.idx = idx;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Word Guessing Game'),
      ),
      body: Container(
        padding: const EdgeInsets.all(20.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
//              SizedBox(height: 10.0,),
              Card(
                color: Colors.white24,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: InkWell(
                  splashColor: Colors.blue.withAlpha(30),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Splash(idx),
                      ),
                    );
                  },
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: 320,
                        height: 500,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Center(child: Text('Instructions', style: TextStyle(fontSize: 45.0, color: Colors.black26, fontWeight: FontWeight.bold),)),
//                            SizedBox(height: 10.0,),
//                            Center(child: Image(image: AssetImage('images/game4.png'), color: Colors.black26, height: 180.0,)),
//                            Text('Games', style: TextStyle(fontSize: 45.0, color: Colors.black26, fontWeight: FontWeight.bold),)
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 30.0,),
              Card(
                color: Colors.white24,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: InkWell(
                  splashColor: Colors.blue.withAlpha(30),
                  onTap: () {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Splash(idx),
                      ),
                    );
                  },
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: 320,
                        height: 70,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text('Start Game', style: TextStyle(fontSize: 30.0, color: Colors.black26, fontWeight: FontWeight.bold),)
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
