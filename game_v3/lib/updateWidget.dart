import 'dart:async';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'dart:io';
import 'update.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';


class updateWidget extends StatefulWidget {
  @override
  _updateWidgetState createState() => _updateWidgetState();
}

class _updateWidgetState extends State<updateWidget> {


  int _state;
  Update up = new Update();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _state = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Update Content"),
      ),
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Padding(
              padding: const EdgeInsets.all(16.0),
              child: new MaterialButton(
                child: setUpButtonChild(),
                onPressed: () {
                  setState(() {
                    if (_state == 0) {
                      _state = 1;
                      Map<dynamic, dynamic> data;
                      http.get('http://172.26.15.97:8000/api').then((http.Response responce) {
                        data = json.decode(responce.body);
                        _write(data);
                      });

                    }
                  });
                },
                elevation: 4.0,
                minWidth: double.infinity,
                height: 48.0,
                color: Colors.lightGreen,
              ),
            ),
          ],
        ),
      ),
    );
  }

  _write(Map<dynamic, dynamic> text) async {
    final Directory directory = await getApplicationDocumentsDirectory();
    final File file = new File(directory.path + '/' + 'wordgamenewfile');
    print(directory.path + '/' + 'wordgamenewfile');
//    file.createSync();
    await file.writeAsString(json.encode(text));
    setState(() {
      _state = 2;
    });
    print('Successfully Downloaded Data');
//    Map<dynamic, dynamic> jsonFileContent = json.decode(file.readAsStringSync());
//    print(jsonFileContent["words"]);
      _read();
  }

  _read() async {
    try {
      final Directory directory = await getApplicationDocumentsDirectory();
      final File file = File(directory.path + '/' + 'wordgamenewfile');
      Map<dynamic, dynamic> jsonFileContent = json.decode(file.readAsStringSync());
      print(jsonFileContent["words"]);
    } catch (e) {
      print("Couldn't read file");
    }
  }


  Widget setUpButtonChild() {
    if (_state == 0) {
      return new Text(
        "Update Content",
        style: const TextStyle(
          color: Colors.white,
          fontSize: 16.0,
        ),
      );
    } else if (_state == 1) {
      return CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
      );
    } else {
      return Icon(Icons.check, color: Colors.white);
    }
  }
}


