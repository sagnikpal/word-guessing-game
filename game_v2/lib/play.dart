import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:game/game.dart';
import 'result.dart';

class Play extends StatefulWidget {
  @override
  _PlayState createState() => _PlayState();
}

class _PlayState extends State<Play> {
  Game game;
  String lives;
  @override
  void initState() {
    super.initState();
    game = Game();
    lives = '6';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text("Guess The Word"),
            SizedBox(width: 140.0,),
            Text("Lives: " + lives),
          ],
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                color: Colors.blueGrey.withOpacity(0.9),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                padding: EdgeInsets.symmetric(horizontal: 105.0, vertical: 10.0),
                child: Text(
                  'Change Word',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 25.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                onPressed: (){
                  setState(() {
                    game = Game();
                  });
                },
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 9.0, vertical: 17.0),
                height: 300.0,
                decoration: BoxDecoration(
                  color: Colors.blueGrey.withOpacity(0.3),
                  borderRadius: BorderRadius.all(Radius.circular(15.0))
                ),
                child: Image.asset('images/unnamed.png'),
              ),
              Text(
                game.displayWord,
                style: TextStyle(fontSize: 48),
              ),
              SizedBox(
                height: 15.0,
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: _getKeyPad(),
              ),
              SizedBox(
                height: 5.0,
              ),
            ],
          ),
        ),
      ),
    );
  }



  Widget _getKeyPad() {
    List<List<String>> letters = [
      ['A', 'B', 'C', 'D', 'E', 'F', 'G'],
      ['H', 'I', 'J', 'K', 'L', 'M', 'N'],
      ['O', 'P', 'Q', 'R', 'S', 'T', 'U'],
      ['V', 'W', 'X', 'Y', 'Z'],
    ];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment:  MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 6.0),
          child: _getRow(0, letters),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 6.0),
          child: _getRow(1, letters),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 6.0),
          child: _getRow(2, letters),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 56.0),
          child: _getRow(3, letters),
        ),

      ],
    );
  }

  Row _getRow(int rowIndex, List<List<String>> letters) {
    List<Widget> row = [];
    for (int i = 0; i < letters[rowIndex].length; i++) {
      row.add(_getLetterButton(letters[rowIndex][i]));
    }
    return Row(
      children: row,
    );
  }

  Widget _getLetterButton(String letter) {
    return Padding(
      padding: const EdgeInsets.all(3.0),
      child: Builder(
        builder: (BuildContext context) {
          return GestureDetector(
            child: Container(
              width: MediaQuery.of(context).size.width / 9.0,
              height: 45,
              alignment: Alignment.center,
              child: Text(
                letter,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 30.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(15)),
                color: _getLetterColor(letter),
              ),
            ),
            onTap: () {
              if (game.displayWordList.contains(letter) ||
                  game.wrongLettersGuessed.contains(letter)) {
                Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text(
                    "Already Guessed",
                    textAlign: TextAlign.center,
                  ),
                  duration: Duration(seconds: 1),
                ));
              } else {
                Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text(
                    letter,
                    textAlign: TextAlign.center,
                  ),
                  duration: Duration(seconds: 1),
                ));
                setState(() {
                  game.guessLetter(letter);
                });
                if (game.isWordGuessed() || game.hasLost()) {
                  Navigator.of(context).pop();
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return Result.fromData(game);
                  }));
                }
              }
              setState(() {
                lives = (6 - game.wrongLettersGuessed.length).toString();
              });
            },
          );
        },
      ),
    );
  }

  Color _getLetterColor(String letter) {
    if (game.displayWordList.contains(letter)) {
      return Colors.green;
    } else if (game.wrongLettersGuessed.contains(letter)) {
      return Colors.red;
    } else {
      return Colors.blueGrey;
    }
  }
}
