

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appName = 'Word Guessing';

    return MaterialApp(
      title: appName,
      theme: ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.dark,
        primaryColor: Colors.lightBlue[800],
        accentColor: Colors.white,

        // Define the default font family.
        fontFamily: 'Merriweather',

        // Define the default TextTheme. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: TextTheme(
          headline6: TextStyle(fontSize: 20.0, fontStyle: FontStyle.italic, fontFamily: 'Sri', color: Colors.black),
        ),
      ),
      home: MyHomePage(
        title: appName,
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final String title;

  MyHomePage({Key key, @required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
        child: Container(
          color: Theme.of(context).accentColor,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[

                Container(
                        padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                        child: Image(
                          image: AssetImage('assets/teamwork.jpg'),
                        )

                ),
             Container(
                 padding: EdgeInsets.fromLTRB(5.0,5.0,5.0,5.0),
                 alignment: Alignment.center,

                 child: Text(
                  '  T   E   A   M   W   O   R   K ',
                  // style: Theme.of(context).textTheme.headline6,
                     textAlign: TextAlign.justify,
                     style: TextStyle(color: Colors.black,fontSize: 30.0,fontWeight:FontWeight.bold )
                 )
             ),

                Container(
                    padding: EdgeInsets.fromLTRB(20.0,10.0,20.0,10.0),
                    child: Text(
                      'The combined action of a group, team work results in effective and efficient progress.It increases collabration and brainstorming.',
                      style: Theme.of(context).textTheme.headline6,
                      textAlign: TextAlign.justify,
                    )
                ),

                Container(
                    padding: EdgeInsets.only(top:5.0,bottom: 5.0),
                    child: Theme(
                      data: Theme.of(context).copyWith(
                       colorScheme: Theme.of(context).colorScheme.copyWith(secondary: Colors.lightBlue[800]),
                      ),
                      child: FloatingActionButton(
                        onPressed:() {},
                        child: Text("Next"),
                        backgroundColor: Colors.black26,


                      ),


                    )
                ),
                Container(
                    padding: EdgeInsets.only(top:5.0,bottom: 5.0),
                    child: Theme(
                      data: Theme.of(context).copyWith(
                        colorScheme: Theme.of(context).colorScheme.copyWith(secondary: Colors.lightBlue[800]),
                      ),
                      child: FloatingActionButton(
                        onPressed:() {},
                        child: Text("Quit"),
                        backgroundColor: Colors.black26,


                      ),


                    )
                ),
              ]
          ),
        ),
     ),
    );
  }
}

class MyApp1 extends StatelessWidget {

  @override
  String title="Word Guessing";
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        centerTitle: true,
      ),
      body: Container(
        padding:EdgeInsets.all(16.0),
        alignment: Alignment.center,
        color: Colors.white,
        child:Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                'INSTRUCTIONS',
                style: TextStyle(color: Colors.black,fontSize: 30.0,fontWeight:FontWeight.bold ),

              ),
            ),
            Container(
                padding: EdgeInsets.fromLTRB(10.0,10.0,10.0,10.0),
                child: Text(
                  '> In this game you need to pick the right option for the color been flashed on the screen.We will note your responses and the time taken to complete the game',
                  style: Theme.of(context).textTheme.headline6,
                  textAlign: TextAlign.justify,

                )
            ),
            Container(
                padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                child: Image(
                  image: AssetImage('assets/mickymouse.jpg'),
                )

            ),
            Container(
                padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                child: RaisedButton(
                  color: Colors.grey,
                  onPressed: (){},
                  padding: const EdgeInsets.all(10.0),
                  child: new Text(
                    "START GAME",
                  ),
                )

            ),
            Container(
                padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                child: RaisedButton(
                  color: Colors.grey,
                  onPressed: (){},
                  padding: const EdgeInsets.all(15.0),
                  child: new Text(
                    "EXIT GAME",
                  ),
                )

            ),
          ],
        )
      ),
    );
  }
}